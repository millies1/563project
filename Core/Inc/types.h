/*
 * types.h
 *
 *  Created on: Apr 13, 2022
 *      Author: Mitchell Illies
 */

#ifndef INC_TYPES_H_
#define INC_TYPES_H_


/* Basic Types*/
#define tU8  (unsigned char)
#define tS8  (signed char)
#define tU16 (unsigned short)
#define tS16 (signed short)
#define tU32 (unsigned int)
#define tS32 (signed int)
#define tU64 (unsigned long long)
#define tS64 (signed long long)
#define tF32 (float)
#define tF64 (double)

/* Boolean Data Type */
#define TRUE  1
#define FALSE 0

/* Return Types */
typedef enum _eReturnValue
{
	eRV_Success =  0x0000,
	eRV_Failure =  0x0001,
} eReturnValue;


#endif /* INC_TYPES_H_ */
