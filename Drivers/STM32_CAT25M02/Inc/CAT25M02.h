/*
 * CAT25M02.h
 *
 *  Created on: Apr 13, 2022
 *      Author: Mitchell Illies
 */

#ifndef STM32_CAT25M02_SRC_CAT25M02_H_
#define STM32_CAT25M02_SRC_CAT25M02_H_

/* Capacity */
#define EEPROM_CAPACITY_CAT25M02	0x40000

/* Commands */
#define EEPROM_CAT25_COMMAND_WRITE_STATUS		0x01 /* Write to the Status Registers */
#define EEPROM_CAT25_COMMAND_WRITE		        0x02 /* Write to an Address in Memory */
#define EEPROM_CAT25_COMMAND_READ		        0x03 /* Read from and Address in Memory */
#define EEPROM_CAT25_COMMAND_READ_STATUS        0x05 /* Read from the Status Registers */
#define EEPROM_CAT25_COMMAND_WRITE_ENABLE       0x06 /* Enable Writing to EEPROM */
#define EEPROM_CAT25_COMMAND_WRITE_DISABLE      0x04 /* Disable Writing to EEPROM */

/* Page Sizes */
#define EEPROM_PAGE_SIZE_CAT25M02	256

/* Write Time/Timeouts */
#define EEPROM_CAT25_MAX_WRITE_TIME_MS		5
#define EEPROM_CAT25_TIMEOUT_TIME_MS		(EEPROM_CAT25_MAX_WRITE_TIME_MS + 1)

typedef enum _eEEPROM_CAT25_Device
{
  CAT25M02,
} eEEPROM_CAT25_Device;

typedef union {
    struct {
        uint8_t  WPEN:1;	// Write Protect Enable. Not present on CAT25040/CAT25020/CAT25010
        uint8_t  IPL:1;		// Identification Page Latch. Present only on newer revisions of CAT25128/CAT25256 and all CAT25512/CAT25M01
	    uint8_t  :1;		// reserved
	    uint8_t  LIP:1;		// Lock Identification Page. Present only on newer revisions of CAT25128/CAT25256 and all CAT25512/CAT25M01
	    uint8_t  BP:2;		// Block protection
	    uint8_t  WEL:1;		// Write Enable Latch
	    uint8_t  RDY:1;		// Ready (ready when low)
    } bit;
    uint32_t word;
} EEPROM_CAT25_Status_Register;

class CAT25M02 {
public:
	CAT25M02();
	virtual ~CAT25M02();
};

#endif /* STM32_CAT25M02_SRC_CAT25M02_H_ */
